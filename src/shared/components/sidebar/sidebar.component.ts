import { Component, ElementRef } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  model: any[] = [];
  constructor(public el: ElementRef){}
  ngOnInit() {
    this.model = [
        {
            label: 'Trang chủ',
            items: [
                { label: 'Trang chủ', icon: 'pi pi-fw pi-home', routerLink: ['/home'] }
            ]
        },
        {
            label: 'Tuyển Dụng',
            routerLink: ['/recruitment'],
            items: [
                {
                    label: 'Vị Trị Tuyển Dụng', icon: 'pi pi-fw pi-users',
                    items: [
                        {
                            label: 'C#', 
                            icon: 'pi pi-fw pi-user',
                            routerLink: ['/job-dotnet']
                        },
                        {
                            label: 'Java', 
                            icon: 'pi pi-fw pi-cog',
                            routerLink: ['/job-java']
                        },
                        {
                          label: 'Angular', 
                          icon: 'pi pi-fw pi-cog',
                          routerLink: ['/job-angular']
                        },
                    ]
                },        
            ]
        },
    ];
  }
}
