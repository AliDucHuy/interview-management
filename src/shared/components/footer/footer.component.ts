import { Component } from '@angular/core';
import { MenuItemService } from '../sidebar/services/menu-item.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
  constructor(public menuService: MenuItemService) { }
}
