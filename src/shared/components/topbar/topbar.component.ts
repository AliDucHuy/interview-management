import { Component, ElementRef, ViewChild } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { MenuItemService } from '../sidebar/services/menu-item.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent {
  items!: MenuItem[];

	@ViewChild('menubutton') menuButton!: ElementRef;

	@ViewChild('topbarmenubutton') topbarMenuButton!: ElementRef;

	@ViewChild('topbarmenu') menu!: ElementRef;

	langOptions: any[] = [
		{
			label: 'EN',
			value: 'en'
		},
		{
			label: 'VI',
			value: 'vi'
		}
	]

	constructor(
		public menuService: MenuItemService) {
	}

	ngOnInit(): void {
	}
}
