import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SidebarComponent } from './sidebar/sidebar.component';
import { TopbarComponent } from './topbar/topbar.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { FooterComponent } from './footer/footer.component';
import { AppMenuitemComponent } from './sidebar/menu-item/menu-item.component';
import { MenuModule } from 'primeng/menu';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { ButtonModule } from 'primeng/button';
import { ImageModule } from 'primeng/image';
import { GalleriaModule } from 'primeng/galleria';
import { TabViewModule } from 'primeng/tabview';
import { FileUploadModule } from 'primeng/fileupload';
import { CheckboxModule } from 'primeng/checkbox';
import { InputSwitchModule } from 'primeng/inputswitch';
import { InputNumberModule } from 'primeng/inputnumber';
import { DropdownModule } from 'primeng/dropdown';
import { TagModule } from 'primeng/tag';
import { ToastModule } from 'primeng/toast';
import { CardModule } from 'primeng/card';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { CalendarModule } from 'primeng/calendar';
import { PanelModule } from 'primeng/panel';
import { TimelineModule } from 'primeng/timeline';
import { PasswordModule } from 'primeng/password';

@NgModule({
  declarations: [
    SidebarComponent,
    TopbarComponent,
    PageNotFoundComponent,
    FooterComponent,
    AppMenuitemComponent
  ],
  imports: [
    BrowserModule,
    MenuModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule,


    ButtonModule,
    ImageModule,
    TabViewModule,
    FileUploadModule,
    CheckboxModule,
    InputSwitchModule,
    InputNumberModule,
    DropdownModule,
    TagModule,
    ToastModule,
    CardModule,
    ToggleButtonModule,
    CalendarModule,
    PanelModule,
    TimelineModule,
    PasswordModule,
  ],
  exports:[
    SidebarComponent,
    TopbarComponent,
    PageNotFoundComponent,
    FooterComponent,
    AppMenuitemComponent,

    ButtonModule,
    ImageModule,
    TabViewModule,
    FileUploadModule,
    CheckboxModule,
    InputSwitchModule,
    InputNumberModule,
    DropdownModule,
    TagModule,
    ToastModule,
    CardModule,
    ToggleButtonModule,
    CalendarModule,
    PanelModule,
    TimelineModule,
    PasswordModule,
  ],
  providers: [],
  bootstrap: []
})
export class SharedModule { }
