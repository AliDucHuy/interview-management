import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { RecruitmentComponent } from './pages/recruitment/recruitment.component';
import { JobJavaComponent } from './pages/recruitment/job-java/job-java.component';
import { JobAngularComponent } from './pages/recruitment/job-angular/job-angular.component';
import { JobDotnetComponent } from './pages/recruitment/job-dotnet/job-dotnet.component';

const routes: Routes = [
  { path: '', redirectTo: "/home", pathMatch:"full" },
  { path: 'home', component: HomeComponent },
  { path: 'recruitment', component: RecruitmentComponent },
  { path: 'job-java', component: JobJavaComponent },
  { path: 'job-angular', component: JobAngularComponent },
  { path: 'job-dotnet', component: JobDotnetComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
