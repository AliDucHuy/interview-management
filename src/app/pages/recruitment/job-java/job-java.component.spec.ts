import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobJavaComponent } from './job-java.component';

describe('JobJavaComponent', () => {
  let component: JobJavaComponent;
  let fixture: ComponentFixture<JobJavaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobJavaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JobJavaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
