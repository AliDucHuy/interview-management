import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobAngularComponent } from './job-angular.component';

describe('JobAngularComponent', () => {
  let component: JobAngularComponent;
  let fixture: ComponentFixture<JobAngularComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobAngularComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JobAngularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
