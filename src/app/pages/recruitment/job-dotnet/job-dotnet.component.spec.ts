import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobDotnetComponent } from './job-dotnet.component';

describe('JobDotnetComponent', () => {
  let component: JobDotnetComponent;
  let fixture: ComponentFixture<JobDotnetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobDotnetComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JobDotnetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
