import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from 'src/shared/components/share.module';
import { HomeComponent } from './pages/home/home.component';
import { RecruitmentComponent } from './pages/recruitment/recruitment.component';
import { JobJavaComponent } from './pages/recruitment/job-java/job-java.component';
import { JobAngularComponent } from './pages/recruitment/job-angular/job-angular.component';
import { JobDotnetComponent } from './pages/recruitment/job-dotnet/job-dotnet.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RecruitmentComponent,
    JobJavaComponent,
    JobAngularComponent,
    JobDotnetComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule
  ],
  exports: [
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
